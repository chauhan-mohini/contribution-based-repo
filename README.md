# Contribution guidelines based Blank Repository
NODE JS version: 14.17.6

## What

It is basically a guidelines based blank repository.[link](https://gitlab.com/Talking-DB/coding/pocs/ui-components/about-ui-component-pocs/-/blob/main/CONTRIBUTING.md)

## Why

To get a hands on experience with the creation of repo following the contribution guidelines provided.

## How

In order to start creating the repo based on the contribution guidelines we have to go through the following steps:

* Identify the UI components and create a gitlab repo.

* Create a react app.

* Installed prettier using the following command.
  
  ``` 
  npm install --save-dev --save-exact prettier
  ```
  
* Created an empty config file using the following command to let editors and other tools know that Prettier is being used.

    ```
    echo {}> .prettierrc.json
    ```

* Configure the .prettierrc as follows:
    ```
    { "tabWidth": 2, "useTabs": false }
    ```
* To help speed up productivity in React projects and stop copying, pasting, and renaming files each time you want to create a new component.Install generate-react-cli.
    ```
    npm i generate-react-cli
    ```
*  To run it using npx use following command.
    ```
    npx generate-react-cli component <name of component>
    ```
* Its configuration is as follows:
    ```
    { "usesTypeScript": false, "usesCssModule": true, "cssPreprocessor": "scss", "testLibrary": "None", "component": { "default": { "path": "src/components", "withStyle": true, "withTest": true, "withStory": true, "withLazy": true } } }
    ```
* ### Initializing Storybook
    To get started installing Storybook, run:
    ```
    npx -p @storybook/cli sb init
    npm run storybook
    ```

