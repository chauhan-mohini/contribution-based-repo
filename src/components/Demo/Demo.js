import React from 'react';
import PropTypes from 'prop-types';
import styles from './Demo.module.scss';

const Demo = () => (
  <div className={styles.Demo}>
    Demo Component
  </div>
);

Demo.propTypes = {};

Demo.defaultProps = {};

export default Demo;
