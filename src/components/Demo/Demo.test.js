import React from 'react';
import ReactDOM from 'react-dom';
import Demo from './Demo';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Demo />, div);
  ReactDOM.unmountComponentAtNode(div);
});