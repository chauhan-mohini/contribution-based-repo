import React, { lazy, Suspense } from 'react';

const LazyDemo = lazy(() => import('./Demo'));

const Demo = props => (
  <Suspense fallback={null}>
    <LazyDemo {...props} />
  </Suspense>
);

export default Demo;
